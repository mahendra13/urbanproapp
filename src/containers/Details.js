import React, { Component } from 'react';
import { Container, Button,Card, Spinner, Text } from 'native-base';
import styles from '../styles';
import {FlatList,Image, View,Linking} from 'react-native';


export default class FirstTab extends Component {

    static navigationOptions = () => ({
        title: 'Details',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: '#2ECC71'
        },
    });

  render() {
    const { state } = this.props.navigation;
    var item = state.params ? state.params.item : "<undefined>";
    console.log(item)
    return (
      <Container>
        <View style = {{margin:20}}>
            <Text numberOfLines={1} style={{color:'#000', marginTop:13, fontSize:20,}}>{item.name}
                <Text style={{color:'red', paddingLeft:15, fontSize:11,}}>  {item.tag}</Text>
            </Text>
            <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:16,}}>{item.classLocPref}</Text>
            <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:16,}}>{item.location}</Text>
            <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:16,}}>{item.categoryName}</Text>            
            <Text numberOfLines={1} style={{color:'#2ECC71', marginTop:3, fontSize:16,}}>{item.phoneNumber}</Text>            
        </View>
      </Container>
    );
  }
}