import React, { Component } from 'react';
import { Container, View, Tab, Tabs, TabHeading, Text } from 'native-base';
import Styles from '../styles';
import Tab1 from '../components/EnquiriesTab';
import Tab2 from '../components/StudentsTab';


export default class Home extends Component {

    static navigationOptions = () => ({
      title: 'Urban Pro',
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#2ECC71'
      },
    });

    firstPressed = (item) => {
      this.props.navigation.navigate('Details', {item})

    }
    secondPressed = (item) => {
      let data = {
        name :item.name,
        tag:item.platformTag,
        classLocPref:item.studentType,
        location:item.batchName,
        categoryName:item.category,
        phoneNumber:item.phoneNumber
      }
      this.props.navigation.navigate('Details', {item:data})
    }

  render() {
    return (
      <Container>
        <Tabs tabBarUnderlineStyle={{ backgroundColor: '#2ECC71' }}  page={2} ref="mytabs" >
          <Tab heading="Enquiries" tabStyle={Styles.whiteBg} textStyle={{color: 'gray'}} activeTabStyle={{backgroundColor: '#fff'}} activeTextStyle={Styles.activeTabText}>
            <Tab1 ref={ref => this.feedback = ref}
           onPress={this.firstPressed}/>
          </Tab>
          <Tab heading="Students" tabStyle={Styles.whiteBg} textStyle={{color: 'gray'}} activeTabStyle={{backgroundColor: '#fff'}} activeTextStyle={Styles.activeTabText}>
            <Tab2 ref={ref => this.feedback = ref}
           onPress={this.secondPressed}/>
          </Tab>
        </Tabs>
      </Container>
    )
  }
}
