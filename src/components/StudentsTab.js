import React, { Component } from 'react';
import { Container,Badge, Button,Card, Spinner, Text } from 'native-base';
import styles from '../styles';
import {FlatList,Image, View,Linking} from 'react-native';
import axios from 'axios';
import RandomColor from '../constants/RandomColor';

export default class FirstTab extends Component {
    constructor(props){
        super(props);
        this.state={
            StudentList:[]
        }
    }

    componentDidMount(){
      this.getApi();
    }

    getApi=()=>{
        let url =  `http://www.mocky.io/v2/5c41950b0f0000543fe7b8a2`;
        axios({
          method: 'get',
          url,
        })
        .then(response => {
          console.log(response.data.dataList)
          this.setState({
            StudentList :response.data.dataList
          })
        })
        .catch(err => {
            console.log(err)
        });
  }
  callBtnPressed=(item)=>{
        Linking.openURL(`tel:+91${item.phoneNumber}`)
  }
  starBtnPressed=(myItem)=>{
    let list = Object.assign([], this.state.StudentList);
    list.map((item,index) =>{
        if(item.enqId ==myItem.enqId){
            list[index].isExclusive = !item.isExclusive
        }
    });
    this.setState({
        StudentList:list
    });
  }

  renderEnqiryList=(item)=>{
    let newColor = RandomColor.colorArray[Math.floor(Math.random() * RandomColor.colorArray.length)]
    return (
        <View style={[Styles.listItem,{ height: 80}]}>
            <Button onPress={()=>this.props.onPress(item)} style = {{height:80, flexDirection:'row', backgroundColor:'#FFF', width:'100%'}}>
                <View style = {{height:80,justifyContent:'flex-start', alignItems:'center',  width:'10%',}}>
                    <Badge style = {{backgroundColor : newColor, marginTop:15, marginLeft:7}}>
                        <Text style = {{color:'#fff', fontWeight:'700'}}>{item.name.charAt(0).toUpperCase()}</Text>
                    </Badge>
                </View>
                <View style = {{height:80, width:'60%',}}>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:13, fontSize:15,}}>{item.name}
                        <Text style={{color:'red', paddingLeft:15, fontSize:11,}}>  {item.platformTag}</Text>
                    </Text>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:13,}}>{item.category}</Text>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:13,}}>{item.batchName}</Text>                    
                </View>
                <View style = {{height:80, width:'30%',}}>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:15, fontSize:10,}}>{item.created}</Text>
                    <Button style = {{height:25, marginRight:10,marginTop:10, backgroundColor:'#2ECC71', alignSelf:'flex-end', justifyContent:'center', alignItems:'center', width:25}} small onPress = {()=>this.callBtnPressed(item)}>
                        <Image source={require('../images/call.png')} style={{ height: 15, width:15 }} />
                    </Button>
                </View>
            </Button>
        </View>
      )
    }
  render() {
    return (
      <Container>
          {this.state.StudentList.length <=0 ?
          <Spinner />
          :
          <FlatList
                data={this.state.StudentList }
                renderItem={({ item }) => this.renderEnqiryList(item)}
                keyExtractor={item => Math.random.toString()}
            />
          }
      </Container>
    );
  }
}