import React, { Component } from 'react';
import { Container, Button,Card,Badge, Spinner, Text } from 'native-base';
import Styles from '../styles';
import {FlatList,Image,TouchableHighlight, View,Linking} from 'react-native';
import axios from 'axios';
import RandomColor from '../constants/RandomColor';

export default class FirstTab extends Component {


    constructor(props){
        super(props);
        this.state={
            EnquiryList:[]
        }
    }

    componentDidMount(){
      this.getApi();
    }

    getApi=()=>{
        let url =  `http://www.mocky.io/v2/5c41920e0f0000543fe7b889`;
        axios({
          method: 'get',
          url,
        })
        .then(response => {
          console.log(response.data.dataList)
          this.setState({
            EnquiryList :response.data.dataList
          })
        })
        .catch(err => {
            console.log(err)
        });
  }

  listClick=(item)=>{
    this.props.navigation.navigate('Details', {item})
  }
  callBtnPressed=(item)=>{
        Linking.openURL(`tel:+91${item.phoneNumber}`)
  }
  starBtnPressed=(myItem)=>{
    let list = Object.assign([], this.state.EnquiryList);
    list.map((item,index) =>{
        if(item.enqId ==myItem.enqId){
            list[index].isExclusive = !item.isExclusive
        }
    });
    this.setState({
        EnquiryList:list
    });
  }

  renderEnqiryList=(item)=>{
    let newColor = RandomColor.colorArray[Math.floor(Math.random() * RandomColor.colorArray.length)]
    let starImagePath = ''
    if(!item.isExclusive){
        starImagePath = require('../images/star_active.png')
    }else{
        starImagePath = require('../images/star_passive.png')
    }
    return (
        <View style={[Styles.listItem,{ height: 120}]}>
            <Button onPress={()=>this.props.onPress(item)} style = {[Styles.listBtn, {height:120}]}>
                <View style = {{height:120,justifyContent:'flex-start', alignItems:'center',  width:'10%',}}>
                    <Badge style = {{backgroundColor : newColor, marginTop:15, marginLeft:7}}>
                        <Text style = {Styles.BadgeTxt}>{item.name.charAt(0).toUpperCase()}</Text>
                    </Badge>
                </View>
                <View style = {{height:120, width:'60%',}}>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:13, fontSize:15,}}>{item.name}
                        <Text style={{color:'red', paddingLeft:15, fontSize:11,}}>  {item.tag}</Text>
                    </Text>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:13,}}>{item.classLocPref}</Text>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:13,}}>{item.location}</Text>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:13,}}>{item.categoryName}</Text>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:3, fontSize:13,}}>Be the first one to respond</Text>
                </View>
                <View style = {{height:120, width:'30%',}}>
                    <Text numberOfLines={1} style={{color:'#000', marginTop:15, fontSize:10,}}>{item.createdOn}</Text>
                    <Button style = {{height:25, marginRight:10,marginTop:10, backgroundColor:'#2ECC71', alignSelf:'flex-end', justifyContent:'center', alignItems:'center', width:25}} small onPress = {()=>this.callBtnPressed(item)}>
                        <Image source={require('../images/call.png')} style={{ height: 15, width:15 }} />
                    </Button>
                    <Button transparent style = {{height:30, marginRight:6, marginTop:20, alignSelf:'flex-end', justifyContent:'center', alignItems:'center', width:30}} small onPress = {()=>this.starBtnPressed(item)}>
                        <Image source={starImagePath} style={{ height: 25, width:25 }} />
                    </Button>
                </View>
            </Button>
        </View>
      )
    }
  render() {
    return (
      <Container>
          {this.state.EnquiryList.length <=0 ?
          <Spinner />
          :
          <FlatList
                data={this.state.EnquiryList }
                renderItem={({ item }) => this.renderEnqiryList(item)}
                keyExtractor={item => item.enqId.toString()}
            />
          }
      </Container>
    );
  }
}