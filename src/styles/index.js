import { StyleSheet, Dimensions } from 'react-native';
import { Platform, StatusBar } from 'react-native';

export default Styles = StyleSheet.create({  
  stretch:{
  },
  activeTabText:{
    color: '#2ECC71', 
    fontWeight: 'bold'
  },
  whiteBg:{
    backgroundColor: '#fff'
  },
  listItem:{
    borderColor: '#cdcdcd', 
    borderWidth: 0.5, 
  },
  listBtn:{
    flexDirection:'row', 
    backgroundColor:'#FFF', 
    width:'100%'
  },
  BadgeTxt:{
    color:'#fff', 
    fontWeight:'700'
  }
  
});

