import React, { Component } from "react";
import Home from './src/containers/Home';
import Details from './src/containers/Details';
import { createStackNavigator,createAppContainer } from 'react-navigation';
const AppNavigator = createStackNavigator({
  Home: { screen: Home },
  Details: { screen: Details },
},
  {
    // headerMode:'none'
    
  });
const App = createAppContainer(AppNavigator);
export default class UrbanPro extends Component {
  render() {
    return (
      <App/>
    );
  }
}